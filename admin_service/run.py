from flask import request
from flask_cors import CORS
from flask import Flask

from admin_service.service.read_service import ReadService

app = Flask(__name__)

@app.route("/detail", methods=["POST"])
def read_context():
    data = request.json
    rs = ReadService()
    result = rs.read_context(unique_link=data['link'])
    return result[1]


@app.route("/create", methods=["GET", "POST"])
def post_data():
    # data = request.get_json()
    # return data
    data = request.json


    return data
if __name__ == '__main__':
    app.run(debug=True, port=12345)


