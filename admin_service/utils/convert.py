import json

class Convert:

    @staticmethod
    def obj2json(obj) -> str:
        """
        对象类生成满足json格式的string，支持嵌套对象
        :param obj:  要转换的obj
        :return:  转换后的满足json格式的string
        """
        var_dict = (obj.__dict__)
        ans = "{"
        cnt = 0
        for i in var_dict:
            if '.' in str(type(var_dict[i])):
                ans = ans + ",\"" + str(i) + "\":"
                tmp = Convert.obj2json(var_dict[i])
                ans += tmp
            else:
                if cnt == 0:
                    ans += "\"" + str(i) + "\":\"" + str(var_dict[i]) + "\""
                else:
                    ans += ",\"" + str(i) + "\":\"" + str(var_dict[i]) + "\""
                cnt += 1
        ans += "}"
        return ans

    @staticmethod
    def json2dict(json_str) -> dict:
        """
        :param json_str: json字符串
        :return: 返回dcit类型数据
        """
        return json.loads(json_str)