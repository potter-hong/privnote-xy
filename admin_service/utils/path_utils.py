import os
import sys


def get_root_path():
    root_path = os.path.abspath(os.path.dirname(__file__)).split('privnote-xy')[0]
    return root_path + "privnote-xy" + get_sepearte()


def get_sepearte():
    sep = "/"
    if sys.platform.lower() == "windows":
        sep = "\\"
    return sep

