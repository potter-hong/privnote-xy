import time
import datetime


class TimeUtils:
    @staticmethod
    def get_time_stamp():
        return str(time.time())

    @staticmethod
    def time_stamp():
        return int(time.time())

    @staticmethod
    def get_today_date():
        """
        获取当天日期
        :return: 格式为yyyymmdd
        """
        today = datetime.datetime.today()
        return today.strftime("%Y%m%d")

    @staticmethod
    def get_t_day_later(t):
        """
        获取间隔t天的日期，格式yyyymmdd
        :param t:
        :return:
        """
        today = datetime.datetime.today()
        t_day = today + datetime.timedelta(days=t)
        return t_day.strftime("%Y%m%d")

    @staticmethod
    def get_t_date_later(day=0, month=0, year=0):
        """
        获取当前时间 t日，m月，y年后的时间
        :param day:
        :param month:
        :param year:
        :return:
        """
        today = datetime.datetime.today()
        if today.month + month > 12:
            month = month - 12
            year = year + 1
        date = today.replace(year=today.year + year, month=today.month + month, day=today.day + day)
        return date.strftime("%Y%m%d")

    @staticmethod
    def get_yesterday_date():
        """
        获取昨天日期
        :return: 格式为yyyymmdd
        """
        today = datetime.datetime.today()
        yesterday = today - datetime.timedelta(days=1)
        return yesterday.strftime("%Y%m%d")

    @staticmethod
    # 格式为yyyy-mm-dd
    def get_today_date_by_detail():
        today = datetime.datetime.today()
        return today.strftime("%Y-%m-%d")

    @staticmethod
    def get_next_day_date_by_detail():
        return TimeUtils.get_t_date_by_detail(-1)

    @staticmethod
    # 格式为yyyy-mm-dd
    def get_t_date_by_detail(t):
        """
        获取t日后的日期，格式yyyy-mm-dd
        :param t:
        :return:
        """
        today = datetime.datetime.today()
        yestd = today - datetime.timedelta(days=t)
        return yestd.strftime("%Y-%m-%d")

    @staticmethod
    def get_recent_week_day():
        today = datetime.datetime.today()
        pre_week_end = today - datetime.timedelta(days=1)
        pre_week_start = today - datetime.timedelta(days=7)
        return (
            today.strftime("%Y-%m-%d+00:00:00"),
            pre_week_start.strftime("%Y%m%d"),
            pre_week_end.strftime("%Y%m%d"),
        )

    @staticmethod
    def get_current_time():
        # 格式为yyyy-mm-dd+hh:mm:ss
        now = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
       # print(now)
        return now

    @staticmethod
    def get_current_time_detail():
        # 格式为yyyy-mm-dd+hh:mm:ss
        now = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
       # print(now)
        return now

    @staticmethod
    def get_next_month(day_s):
        # 获取下个月特定的某一号返回格式 eg:20210801
        today = datetime.datetime.today()
        year = today.year
        month = today.month
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        res = datetime.datetime(year, month, day_s).strftime("%Y%m%d")
        return res

    @staticmethod
    def get_next_day():
        # 获取下一天 yyyymmdd
        next_day = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime(
            "%Y%m%d"
        )
        return next_day

    @staticmethod
    def get_next_week(weekday):
        # 获取某个星期特定的一天，比如周一, 返回格式 yyyymmdd
        delta = weekday - datetime.datetime.now().isoweekday()
        if delta <= 0:
            delta += 7
        return (datetime.datetime.now() + datetime.timedelta(delta)).strftime("%Y%m%d")

    @staticmethod
    def get_next_double_week(weekday):
        # 获取双周星期特定的一天，比如周一, 返回格式 yyyymmdd
        delta = weekday - datetime.datetime.now().isoweekday()
        if delta <= 0:
            delta += 14
        return (datetime.datetime.now() + datetime.timedelta(delta)).strftime("%Y%m%d")

    @staticmethod
    def get_start2end_date(start_datetime, end_datetime):
        """
        输出两个时间段之间，所有的日期，包括始末时间，格式yyyymmdd
        :param start_datetime: 开始日期，如20210910
        :param end_datetime:  结束日期， 如20201912
        :return: 日期列表 [20210910, 20210911, 20210912]
        """
        date1 = time.strptime(start_datetime, "%Y%m%d")
        date1_datetime = datetime.datetime(date1[0], date1[1], date1[2])
        date2 = time.strptime(end_datetime, "%Y%m%d")
        date2_datetime = datetime.datetime(date2[0], date2[1], date2[2])
        count = (date2_datetime - date1_datetime).days
        datetime_list = [date1_datetime.strftime("%Y%m%d")]
        while (count > 0):
            date1_datetime = date1_datetime + datetime.timedelta(1)
            datetime_list.append(date1_datetime.strftime("%Y%m%d"))
            count -= 1
        return datetime_list

