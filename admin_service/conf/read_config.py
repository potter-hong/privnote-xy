from configparser import ConfigParser

from admin_service.utils.path_utils import *


def get_conf_read():
    conf_path = get_root_path() + "admin_service" + get_sepearte() + "conf" + get_sepearte() + "conf.ini"
    cf = ConfigParser()
    cf.read(conf_path)
    return cf


def get_db_info():
    cf = get_conf_read()
    host = cf.get("db", "host")
    port = int(cf.get("db", "port"))
    user = cf.get("db", "user")
    password = cf.get("db", "password")
    return [host, port, user, password]
