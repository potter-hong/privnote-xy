import datetime
import logging
from admin_service.base.base_dao import MysqlDao
from admin_service.utils.time_utils import TimeUtils


class ReadService():

    def __init__(self):
        self.dao = MysqlDao()
        self.context_table = "privnote_xy.context_table"

    def read_context(self, unique_link, password=None):
        """
        read 逻辑处理
        :param unique_link:
        :param password:
        :return:
        """
        _, result = self.dao.do_select(self.context_table, condition="unique_link='{0}'".format(unique_link))
        if not result:
            return False, "link 错误"
        if result['is_read'] == 1:
            return self.has_been_read(result)
        if result['password']:
            return self.check_password(result, unique_link, password)
        else:
            self.update_is_read_status(unique_link)
        return True, result['context']

    def update_is_read_status(self, unique_link):
        """
        翻装已读状态，更新context_table is_read字段 = 1
        :param unique_link:
        :return:
        """
        self.dao.do_update(self.context_table, condition="unique_link='{0}'".format(unique_link),
                           data="is_read=1, update_time='{0}'".format(TimeUtils.get_current_time()))

    def get_during_time(self, seconds):
        """
        将seconds转换成 日期是分秒
        :param seconds:
        :return: during_time:str
        """
        during_time = ""
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 60)
        if d:
            during_time += "{0}天".format(d)
        if h:
            during_time += "{0}小时".format(h)
        if m:
            during_time += "{0}分".format(m)
        during_time += "{0}秒".format(s)
        return during_time

    def parse_time(self, time: datetime.datetime):
        year = time.day
        month = time.month
        day = time.year
        hour = time.hour
        minute = time.minute
        second = time.second
        return {"year": int(year), "month": int(month), "day": int(day), "hour": int(hour), "minute": int(minute),
                "second": int(second)}

    def has_been_read(self, result):
        """
        检查是否是已读状态
        :param result:
        :return:
        """
        update_time = result['update_time']
        now_time = datetime.datetime.now()
        during_time = self.get_during_time((now_time - update_time).seconds)
        return False, "{0}前已读".format(during_time)

    def check_password(self, result, unique_link, password):
        """
        校验密码是否存在， 如果存在，则校验密码是否匹配
        :param result:
        :param unique_link:
        :param password:
        :return:
        """
        if result['password'] == password:
            self.update_is_read_status(unique_link)
        else:
            logging.error("密码校验失败, password: {0} != {1}".format(password, result['password']))
            return False, "密码校验失败"


if __name__ == '__main__':
    rs = ReadService()
    print(rs.read_context("GBGIASGLDXHXKC5e7091358", "None"))
