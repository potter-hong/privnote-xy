import random
import string

from admin_service.base.base_dao import MysqlDao
from admin_service.utils.time_utils import TimeUtils


class CreateService():
    def __init__(self):
        self.dao = MysqlDao()
        self.context_table = "privnote_xy.context_table"

    def create(self, context, password=None):
        """
        创建接口的主要逻辑，接收前端传过来的内容，并存到db中，并生成内容对应的唯一链接
        :param context:
        :param password:
        :return: link唯一链接
        """
        link = self.insert_context(context, password)
        return link

    def insert_context(self, context, password):
        link = self.generate_unique_link()
        data = {"context": context, "create_time": TimeUtils.get_current_time(), "create_type": 1, "is_read": 0,
                "password": password,
                "unique_link": link}
        self.dao.do_insert(self.context_table, data)
        return link

    def generate_unique_link(self):
        """
        生成唯一链接逻辑，采用当前时间进行字母算法匹配随机 + 随机生成9个字母数字混合
        :return:  唯一链接link
        """
        current_time = TimeUtils.get_current_time_detail()
        a_z_letter = {0: 'ABC', 1: 'DEF', 2: 'GHI', 3: 'JKL', 4: 'MNO', 5: 'PQR', 6: 'STU', 7: 'VW', 8: 'XY', 9: "Z"}
        unique_link_str = ""
        for time_char in current_time:
            unique_link_str += random.choice(a_z_letter[int(time_char)])
        m = random.randint(1, 9)
        a = "".join([str(random.randint(0, 9)) for _ in range(m)])
        b = "".join([random.choice(string.ascii_letters) for _ in range(9 - m)])
        unique_link_str += ''.join(random.sample(list(a + b), 9))
        return unique_link_str


if __name__ == '__main__':
    cs = CreateService()
    link = cs.create("xy")
