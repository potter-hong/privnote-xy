import requests

from admin_service.base.http_client import HttpClient


class BaseRequest():
    pass


class BaseResponse():
    def __init__(self):
        self.retcode = ""
        self.retmsg = ""


class BaseClient():
    def __init__(self, ip, port):
        self.client = HttpClient(ip, port, timeout=20)
        self.header = {}

    def send(self, uri, data, method="get"):
        retcode, retmsg = 0, ""
        if method.lower() == 'get':
            retcode, retmsg = self.client.get(data, uri)
        elif method.lower() == "post":
            retcode, retmsg = self.client.post(data, uri)
        return retcode, retmsg


if __name__ == '__main__':
    client = BaseClient("127.0.0.1", 12345)
    result = client.send("detail", {"link": "GBGIASGLDXHXKC5e7091358"}, method="post")
    print(result[1].decode())
