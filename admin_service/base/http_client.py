

import requests
import json
import logging
import traceback


class HttpClient(object):
    def __init__(self, host, port, timeout):
        self.session = requests.session()
        self.host = host
        self.port = port
        self.timeout = timeout

        self.headers = {'Content-type': 'application/json', 'charset': 'utf-8'}

    def post(self, data, uri, **kwargs):
        """
        发送HTTP Post请求, dict格式
        """
        return self.post_str(json.dumps(data), uri, **kwargs)

    def post_str(self, data_str, uri, **kwargs):
        """
        发送HTTP Post请求, str格式
        """
        url = "http://{0}:{1}/{2}".format(self.host, self.port, uri)
        try:
            logging.debug("req url:{0},data:{1}".format(url, data_str))
            response = self.session.post(url, data=data_str, headers=self.headers, timeout=self.timeout, **kwargs)
            if response.status_code != 200:
                logging.error("query url:{0} reqData:{1} failed, code:{2}, msg:{3}".
                              format(url, data_str, response.status_code, response.content))
                return response.status_code, response.content

            logging.debug('url:{0}, reqData:{1}, rspData:{2}'.format(url, data_str, response.content))
            return response.status_code, response.content

        except Exception as err:  # pylint: disable=broad-except
            logging.error("{0}|Fatal Error: {1}".format(url, err))
            logging.exception(err)
            return -1, str(err)

    def get(self, data, uri, **kwargs):
        """
        发送HTTP Get请求
        """
        url = "http://{0}:{1}/{2}".format(self.host, self.port, uri)
        try:
            response = self.session.get(url, params=data, cookies=self.session.cookies,
                                        headers=self.headers,
                                        timeout=self.timeout, **kwargs)
            logging.info("url:" + url)
            logging.info("reqData:" + data)
            if response.status_code != 200:
                logging.error("query failed, code:{2}, msg:{3}".
                              format(url, data, response.status_code, response.content))
                return response.status_code, response.content

            logging.info('rspData:{2}'.format(url, data, response.content))
            return response.status_code, response.content
        except Exception as err:  # pylint: disable=broad-except
            logging.error("{0}|Fatal Error: {1}".format(url, err))
            logging.exception(err)
            return -1, str(err)

    def set_headers(self, headers: dict):
        """
        自定义HTTP HEADER
        """
        self.headers = headers
