import pymysql
import logging

from admin_service.conf.read_config import get_db_info


class MysqlDao():
    def __init__(self):
        self.host = ""
        self.port = 3306
        self.user = ""
        self.password = ""
        self.connect = self.get_connect()

    def get_db_info(self):
        """
        从配置文件中读取db配置信息
        :return:
        """

        self.host, self.port, self.user, self.password = get_db_info()

    def get_connect(self):
        """
        建立db连接
        :return: 连接的游标
        """
        self.get_db_info()
        connect = pymysql.Connection(host=self.host, port=self.port, user=self.user, password=self.password,
                                     charset="utf8", cursorclass=pymysql.cursors.DictCursor)
        return connect

    def get_cursor(self):
        return self.connect.cursor()

    def do_update(self, table_name, data, condition=None):
        cursor = self.get_cursor()
        sql = "update {0} set {1}".format(table_name, data)
        if condition:
            sql += " where {0}".format(condition)
        try:
            cursor.execute(sql)
            self.connect.commit()
            affect_row = cursor.rowcount
            return affect_row
        except Exception as e:
            logging.error(e)
            return -3, f"mysql update error: {repr(e)}"
        finally:
            cursor.close()

    def do_select_many(self, table_name, flied=None, condition=None, limit=None):
        sql = "select * from {0}".format(table_name)
        cursor = self.get_cursor()
        if flied:
            sql = "select {0} from {1}".format(flied, table_name)
        if condition:
            sql += " where {2}".format(flied, table_name, condition)
        if limit:
            sql += " limit {0}".format(limit)
        try:
            cursor.execute(sql)
            resutl = cursor.fetchall()
            return 0, resutl
        except Exception as e:
            logging.error(e)
            return -1, f"mysql query error {repr(e)}"
        finally:
            cursor.close()

    def do_select(self, table_name, flied=None, condition=None, limit=None):
        sql = "select * from {0}".format(table_name)
        cursor = self.get_cursor()
        if flied:
            sql = "select {0} from {1}".format(flied, table_name)
        if condition:
            sql += " where {2}".format(flied, table_name, condition)
        if limit:
            sql += " limit {0}".format(limit)
        try:
            cursor.execute(sql)
            resutl = cursor.fetchone()
            return 0, resutl
        except Exception as e:
            logging.error(e)
            return -1, f"mysql query error {repr(e)}"
        finally:
            cursor.close()

    def do_delete(self, sql):
        pass

    def do_insert(self, table_name, data):
        # todo sql与操作分离
        cursor = self.get_cursor()
        sql = "INSERT INTO %s SET " % table_name
        first_item = True
        for key, value in data.items():
            if first_item:
                sql = sql + key + "='" + str(value) + "'"
                first_item = False
            else:
                sql = sql + "," + key + "='" + str(value).replace("\'", "\"") + "'"
        logging.info("sql: %s" % sql)
        try:
            cursor.execute(sql)
            self.connect.commit()
            affect_row = cursor.rowcount
            return affect_row
        except Exception as e:
            logging.error(e)
            return -2, f"mysql insert error: {repr(e)}"
        finally:
            cursor.close()

    def close(self):
        try:
            self.connect.close()
        except Exception as e:
            logging.exception(e)
            logging.error("mysql close error: %s", repr(e))
